# kanri-kirai
Because we hate content management systems (CMS) but they keep asking us to make one but won't allow us to use a CMS like Wordpress or Drupal.

## Let's make a CMS with Ruby on Rails! Yay!~ (sarcasm)
### System Requirements
#### RVM, Ruby and Rails (#justrubythings)
```
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable
rvm install 2.2.3
rvm 2.2.3
gem install rails
```

#### PostgreSQL 9.x
Refer to [this guide](http://lmgtfy.com/?q=how+do+I+install+postgresql).

#### ImageMagick
Refer to [this guide](http://lmgtfy.com/?q=how+to+install+imagemagick).

#### Elastic Search
Refer to [this guide](http://lmgtfy.com/?q=how+to+install+elasticsearch).


### Generating the application
```
rails new shitty_cms_project -m https://bitbucket.org/usyd-techlab/kanri-kirai/raw/master/composer.rb
```

Follow the prompts. Easy.

Turn on elasticsearch on another terminal:
```
elasticsearch
```

Starting the application:

```
cd shitty_cms_project
rails s
```

The application should be up and running at http://localhost:3000

### Create admin user
Inside your project directory:
```
rake casein:users:create_admin email=techlab@sydney.edu.au password=Pass.123
```

### Adding a new content type to the system
Inside your project directory:
```
rails g techlab:scaffold shitty_content title:string content:text tags:text
rake db:migrate
```
