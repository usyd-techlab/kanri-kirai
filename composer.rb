def say_custom(tag, text); say "\033[1m\033[36m" + tag.to_s.rjust(10) + "\033[0m" + "  #{text}" end
def say_loud(tag, text); say "\033[1m\033[36m" + tag.to_s.rjust(10) + "  #{text}" + "\033[0m" end
def say_recipe(name); say "\033[1m\033[36m" + "recipe".rjust(10) + "\033[0m" + "  Running #{name} recipe..." end
def say_wizard(text); say_custom(@current_recipe || 'khanhposer', text) end

def ask_wizard(question)
  ask "\033[1m\033[36m" + ("option").rjust(10) + "\033[1m\033[36m" + "  #{question}\033[0m"
end

def whisper_ask_wizard(question)
  ask "\033[1m\033[36m" + ("choose").rjust(10) + "\033[0m" + "  #{question}"
end

def yes_wizard?(question)
  answer = ask_wizard(question + " \033[33m(y/n)\033[0m")
  case answer.downcase
  when "yes", "y"
    true
  when "no", "n"
    false
  else
    yes_wizard?(question)
  end
end

def no_wizard?(question); !yes_wizard?(question) end

def copy_from_repo(filename, opts = {})
  repo = 'https://bitbucket.org/usyd-techlab/kanri-kirai/raw/master/files/'
  source_filename = filename
  destination_filename = filename
  begin
    remove_file destination_filename
    get repo + source_filename, destination_filename
  rescue OpenURI::HTTPError
    say_wizard "Unable to obtain #{source_filename} from the repo #{repo}"
  end
end

# full credit to @mislav in this StackOverflow answer for the #which() method:
# - http://stackoverflow.com/a/5471032
def which(cmd)
  exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
  ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
    exts.each do |ext|
    exe = "#{path}#{File::SEPARATOR}#{cmd}#{ext}"
      return exe if File.executable? exe
    end
  end
  return nil
end

HOST_OS = RbConfig::CONFIG['host_os']
say_wizard "Your operating system is #{HOST_OS}."
say_wizard "You are using Ruby version #{RUBY_VERSION}."
say_wizard "You are using Rails version #{Rails::VERSION::STRING}."


############### CONFIGURE GEM FILE START ###############
insert_into_file('Gemfile', "ruby '#{RUBY_VERSION}'\n", :before => /^ *gem 'rails'/, :force => false)
gsub_file 'Gemfile', /gem 'sdoc',\s+'~> 0.4.0',\s+group: :doc/, ''
gsub_file 'Gemfile', /gem 'sqlite3'\n/, ''

say_wizard "Adding gems to Gemfile"
gem 'bootstrap-sass'
gem 'therubyracer'
gem 'pg'
gem 'simple_form'
gem 'font-awesome-rails'
gem 'parsley-rails'
gem 'owlcarousel-rails'
gem 'truncate_html', '~> 0.3.2'
gem 'casein', :git => 'https://bitbucket.org/usyd-techlab/techlab_casein.git'
gem 'bootsy'
gem 'carrierwave'
gem 'select2-rails'
gem 'searchkick'
gem 'will_paginate', '~> 3.0.5'
gem 'bootstrap-will_paginate', '~> 0.0.10'
gem 'dropzonejs-rails'

gem_group :development do
  gem 'better_errors'
  gem 'quiet_assets'
  gem 'rails_layout'
  gem 'spring-commands-rspec'
  gem 'betterlorem', '~> 0.1.2'
end

gem_group :test do
  gem 'capybara'
  gem 'database_cleaner'
  gem 'launchy'
  gem 'selenium-webdriver'
end

gem_group :development, :test do
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'pry-rails'
  gem 'pry-rescue'
  gem 'rspec-rails'
end

rvmrc_detected = false
if File.exist?('.rvmrc')
  rvmrc_file = File.read('.rvmrc')
  rvmrc_detected = rvmrc_file.include? app_name
end
if File.exist?('.ruby-gemset')
  rvmrc_file = File.read('.ruby-gemset')
  rvmrc_detected = rvmrc_file.include? app_name
end
unless rvmrc_detected
  if which("rvm")
    say_wizard "recipe creating project-specific rvm gemset and .rvmrc"
    if ENV['MY_RUBY_HOME'] && ENV['MY_RUBY_HOME'].include?('rvm')
      begin
        gems_path = ENV['MY_RUBY_HOME'].split(/@/)[0].sub(/rubies/,'gems')
        ENV['GEM_PATH'] = "#{gems_path}:#{gems_path}@global"
        require 'rvm'
        RVM.use_from_path! File.dirname(File.dirname(__FILE__))
      rescue LoadError
        raise "RVM gem is currently unavailable."
      end
    end
    say_wizard "creating RVM gemset '#{app_name}'"
    RVM.gemset_create app_name
    say_wizard "switching to gemset '#{app_name}'"
    # RVM.gemset_use! requires rvm version 1.11.3.5 or newer
    rvm_spec =
      if Gem::Specification.respond_to?(:find_by_name)
        Gem::Specification.find_by_name("rvm")
      else
        Gem.source_index.find_name("rvm").last
      end
      unless rvm_spec.version > Gem::Version.create('1.11.3.4')
        say_wizard "rvm gem version: #{rvm_spec.version}"
        raise "Please update rvm gem to 1.11.3.5 or newer"
      end
    begin
      RVM.gemset_use! app_name
    rescue => e
      say_wizard "rvm failure: unable to use gemset #{app_name}, reason: #{e}"
      raise
    end
    if File.exist?('.ruby-version')
      say_wizard ".ruby-version file already exists"
    else
      create_file '.ruby-version', "#{RUBY_VERSION}\n"
    end
    if File.exist?('.ruby-gemset')
      say_wizard ".ruby-gemset file already exists"
    else
      create_file '.ruby-gemset', "#{app_name}\n"
    end
  else
    say_wizard "WARNING! RVM does not appear to be available."
  end
end

say_wizard "Installing Bundler (in case it is not installed)."
run 'gem install bundler'
say_wizard "Installing gems. This will take a while."
run 'bundle install --without production'

############### CONFIGURE GEM FILE END ###############

############### CONFIGURE PostgreSQL START ###############
copy_from_repo 'config/database.yml'
begin
  say_wizard "Creating a user named '#{app_name}' for PostgreSQL"
  run "createuser --createdb #{app_name}"
  gsub_file "config/database.yml", /username: .*/, "username: #{app_name}"
rescue StandardError => e
  raise "unable to create a user for PostgreSQL, reason: #{e}"
end
gsub_file "config/database.yml", /database: myapp_development/, "database: #{app_name}_development"
gsub_file "config/database.yml", /database: myapp_test/,        "database: #{app_name}_test"
gsub_file "config/database.yml", /database: myapp_production/,  "database: #{app_name}_production"


if (yes_wizard? "Okay to drop all existing databases named #{app_name}? 'No' will abort immediately!")
  run 'bundle exec rake db:drop'
else
  raise "aborted at user's request"
end

run 'bundle exec rake db:create:all'
############### CONFIGURE PostgreSQL END ###############

############### #justfrontendthings START ###############
say_wizard "Setting up Bootstrap 3"
generate 'layout:install bootstrap3 -f'
generate 'simple_form:install --bootstrap'

say_wizard "Installing bootsy (WYSIWYG editor)"
generate 'bootsy:install'
run 'bundle exec rake bootsy:install:migrations'
run 'bundle exec rake db:migrate'

say_wizard "Updating Javascript header"
inject_into_file 'app/assets/javascripts/application.js', after: "//= require bootstrap-sprockets\n" do <<-'CODE'
//= require owl.carousel
//= require select2
CODE
end

say_wizard "Updating CSS header"
inject_into_file 'app/assets/stylesheets/application.css.scss', after: "*= require_tree .\n" do <<-'CODE'
 *= require bootsy
 *= require font-awesome
 *= require owl.carousel
 *= require owl.theme
CODE
end

inject_into_file 'app/assets/stylesheets/application.css.scss', after: "*/\n" do <<-'CODE'
#owl-demo .item img{
  display: block;
  width: 100%;
  height: auto;
  overflow: hidden;
  position: relative;
}

.model-title {
  margin-left: 30px;
}

.model-title:after {
  content: '';
  display: block;
  width: 60px;
  height: 1px;
  margin: 15px 0 23px;
  background: #d95204;
  text-align: left;
}

.heart-color {
  color: #FFA4B4;
}
CODE
end

say_wizard "Adding CMS theme stylesheet"
copy_from_repo 'app/assets/stylesheets/cms_theme.css.scss'
copy_from_repo 'app/assets/stylesheets/1st_load_framework.css.scss'

say_wizard "Adding slider images"
copy_from_repo 'public/fullimage1.jpg'
copy_from_repo 'public/fullimage2.jpg'
copy_from_repo 'public/fullimage3.jpg'

say_wizard "Adding layout files"
copy_from_repo 'app/views/layouts/_content.html.erb'
copy_from_repo 'app/views/layouts/_footer.html.erb'
copy_from_repo 'app/views/layouts/_navigation_links.html.erb'
copy_from_repo 'app/views/layouts/_navigation.html.erb'
copy_from_repo 'app/views/layouts/_show_content.html.erb'
copy_from_repo 'app/views/layouts/_sidebar.html.erb'
copy_from_repo 'app/views/layouts/application.html.erb'

say_wizard "Adding home page"
copy_from_repo 'app/views/visitors/index.html.erb'

say_wizard "Adding tags page"
copy_from_repo 'app/views/tags/show.html.erb'

say_wizard "Adding search page"
copy_from_repo 'app/views/search/index.html.erb'
copy_from_repo 'app/views/search/index.js.erb'

say_wizard "Adding Javascript libraries"
copy_from_repo 'app/assets/javascripts/paginate.js.coffee'
copy_from_repo 'app/assets/javascripts/theia-sticky-sidebar.js'

############### #justfrontendthings END ###############

###############  Add models START ###############
say_wizard "Adding content model"
copy_from_repo 'app/models/content.rb'

say_wizard "Adding search model"
copy_from_repo 'app/models/search.rb'

say_wizard "Adding Tag model"
generate "model Tag name:string count:integer"

inject_into_file 'app/models/tag.rb', after: "class Tag < ActiveRecord::Base\n" do <<-'CODE'
  validates_uniqueness_of :name
  # decrement the tag count and if it's zero delete it
  def decrement_or_delete
    self.count = self.count - 1
    if self.count <= 0
      self.delete
    else
      self.save
    end
  end

  # invoked when a post is created or updated
  def self.update_and_clean(old_tags, new_tags)
    to_add = new_tags - old_tags
    to_decrement = old_tags - new_tags

    to_decrement.each do |tag|
      find_by(name: tag).decrement_or_delete
    end

    to_add.each do |tag|
      tag = find_or_create_by(name: tag) do |t|
        t.count = 0
      end
      tag.update(count: tag.count + 1)
    end
  end
CODE
end

run 'bundle exec rake db:migrate'

###############  Add controllers END ###############

###############  Add uploaders START ###############
say_wizard "Adding uploaders"
copy_from_repo 'app/uploaders/document_uploader.rb'
###############  Add uploaders END ###############

###############  Add initializer START ###############
initializer 'kanri_kirai.rb', <<-CODE
CMS_MODELS = []
#SCAFFOLD_INSERT#
CODE
###############  Add initializer START ###############

###############  Add controllers START ###############
say_wizard "Adding application and visitors controller"
copy_from_repo 'app/controllers/application_controller.rb'
copy_from_repo 'app/controllers/visitors_controller.rb'
copy_from_repo 'app/controllers/tags_controller.rb'
copy_from_repo 'app/controllers/search_controller.rb'
###############  Add controllers END ###############

############### Add Techlab CMS generator START ###############
say_wizard "Adding techlab:scaffold generator"
copy_from_repo 'lib/generators/techlab/scaffold/templates/views/index.html.erb'
copy_from_repo 'lib/generators/techlab/scaffold/templates/views/index.js.erb'
copy_from_repo 'lib/generators/techlab/scaffold/templates/views/show.html.erb'
copy_from_repo 'lib/generators/techlab/scaffold/templates/controller.rb'
copy_from_repo 'lib/generators/techlab/scaffold/templates/migration.rb'
copy_from_repo 'lib/generators/techlab/scaffold/templates/model.rb'
copy_from_repo 'lib/generators/techlab/scaffold/scaffold_generator.rb'
############### Add Techlab CMS generator END ###############

############### Routing START ###############
say_wizard "Setting up routes"
route "root to: 'visitors#index'"
route "get 'tag/:tag', to: 'tags#show', as: 'tag'"
route "get 'search', to: 'search#index', as: 'search'"
############### Routing END ###############

############### CLEAN UP START ###############
say_wizard "Cleaning up files from default rails generator"
%w{
  public/index.html
  app/assets/images/rails.png
}.each { |file| remove_file file }

say_wizard "Cleaning up Gemfile"
gsub_file 'Gemfile', /#.*\n/, "\n"
gsub_file 'Gemfile', /\n^\s*\n/, "\n"

say_wizard "Cleaning up routes.rb"
gsub_file 'config/routes.rb', /  #.*\n/, "\n"
gsub_file 'config/routes.rb', /\n^\s*\n/, "\n"

############### CLEAN UP END ###############

generate 'casein:install -f'
run 'bundle exec rake db:migrate'
