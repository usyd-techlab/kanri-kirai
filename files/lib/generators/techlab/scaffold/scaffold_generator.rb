module Techlab
  class ScaffoldGenerator < Rails::Generators::NamedBase
    include Rails::Generators::Migration
    source_root File.expand_path('../templates', __FILE__)

    argument :attributes, :type => :array, :required => true, :desc => "attribute list required"

    class_options :create_casein_files => false

    def self.next_migration_number dirname
      if ActiveRecord::Base.timestamped_migrations
        Time.now.utc.strftime("%Y%m%d%H%M%S")
      else
        "%.3d" % (current_migration_number(dirname) + 1)
      end
    end

    def generate_files
      @plural_route = (plural_name != singular_name) ? plural_name : "#{plural_name}_index"
      @read_only = options[:read_only]
      @no_index = options[:no_index]

      template 'controller.rb', "app/controllers/#{plural_name}_controller.rb"
      template 'views/index.html.erb', "app/views/#{plural_name}/index.html.erb"
      template 'views/index.js.erb', "app/views/#{plural_name}/index.js.erb"
      template 'views/show.html.erb', "app/views/#{plural_name}/show.html.erb"

      add_to_routes
      add_to_navigation
      add_to_app_controller

      template 'model.rb', "app/models/#{singular_name}.rb"
      migration_template 'migration.rb', "db/migrate/create_#{plural_name}.rb"

      call_techlab_casein
    end

    protected

    def add_to_routes
      puts "   techlab_cms     adding #{plural_name} resources to routes.rb"
      file_to_update = Rails.root + 'config/routes.rb'
      insert_sentinel = 'Application.routes.draw do'
      line_to_add = "resources :#{plural_name}, :only => [:index, :show] # techlab_cms generated"
      gsub_add_once plural_name, file_to_update, "\t" + line_to_add, insert_sentinel
    end

    def add_to_navigation
      puts "   techlab_cms     adding #{plural_name} to navigation links"
      file_to_update = Rails.root + 'app/views/layouts/_navigation_links.html.erb'
      line_to_add = "<li><%= link_to \"#{plural_name.humanize.capitalize}\", #{@plural_route}_path %></li>"
      insert_sentinel = '<!-- SCAFFOLD_INSERT -->'
      gsub_add_once plural_name, file_to_update, line_to_add, insert_sentinel
    end

    def add_to_app_controller
      puts "   techlab_cms     adding #{plural_name} to app controller"
      file_to_update = Rails.root + 'config/initializers/kanri_kirai.rb'
      line_to_add = "CMS_MODELS.push(#{class_name})"
      insert_sentinel = '#SCAFFOLD_INSERT#'
      gsub_add_once plural_name, file_to_update, line_to_add, insert_sentinel
    end

    def call_techlab_casein
      attrs = [singular_name]
      attributes.each do |attr|
        attrs.push "#{attr.name}:#{attr.type}"
      end

      Rails::Generators.invoke("casein:scaffold", attrs)
    end

    def gsub_add_once m, file, line, sentinel
      unless options[:pretend]
        gsub_file file, /(#{Regexp.escape("\n#{line}")})/mi do |match|
          ''
        end
        gsub_file file, /(#{Regexp.escape(sentinel)})/mi do |match|
          "#{match}\n#{line}"
        end
      end
    end

    def gsub_file(path, regexp, *args, &block)
      content = File.read(path).gsub(regexp, *args, &block)
      File.open(path, 'wb') { |file| file.write(content) }
    end

  end
end
