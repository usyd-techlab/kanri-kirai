class TagsController < ApplicationController
  before_action :set_post, only: [:show]

  def show
    redirect_to root_path if @tag.nil?

    @tagged_content = []

    CMS_MODELS.each do |cms_model|
      @tagged_content += cms_model.where('tags LIKE ? OR tags LIKE ? OR tags LIKE ? OR tags LIKE ?', "#{@tag.name},%", "%, #{@tag.name}, %", "#{@tag.name}", "%, #{@tag.name}").map do |entity|
        Content.new(title: entity.title, content: entity.content, created_at: entity.created_at, path: send("#{cms_model.name.downcase}_path", entity), model_name: cms_model.name, tags: entity.tags)
      end
    end

    @tagged_content = @tagged_content.sort_by(&:created_at).reverse

  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @tag = Tag.find_by(name: params[:tag])
  end
end
